<?php
//error_reporting(0);
session_start();
$mod = $_GET['mod'];
switch ($_SESSION['role']) {
	case 'admin':
?>
	<ul class="nav navbar-nav">
		<li <?php if ($mod=="") echo('class="active"'); ?> >
			<a href="?mod=">Beranda</a>
		</li>
		<li <?php if ($mod=="semester") echo('class="active"'); ?> >
			<a href="?mod=semester">Semester</a>
		</li>
		<li <?php if ($mod=="thn_akademik") echo('class="active"'); ?> >
			<a href="?mod=thn_akademik">Tahun Akademik</a>
		</li>
		<li <?php if ($mod=="fakultas") echo('class="active"'); ?> >
			<a href="?mod=fakultas">Fakultas</a>
		</li>
		<li <?php if ($mod=="jurusan") echo('class="active"'); ?> >
			<a href="?mod=jurusan">Jurusan</a>
		</li>
		<li <?php if ($mod=="mahasiswa") echo('class="active"'); ?> >
			<a href="?mod=mahasiswa">Mahasiswa</a>
		</li>
		<li <?php if ($mod=="persebaran") echo('class="active"'); ?> >
			<a href="?mod=persebaran">Persebaran</a>
		</li>
		<li <?php if ($mod=="promosi") echo('class="active"'); ?> >
			<a href="?mod=promosi">Promosi</a>
		</li>
		<li <?php if ($mod=="persebaranPromosi") echo('class="active"'); ?> >
			<a href="?mod=persebaranPromosi">Persebaran Promosi</a>
		</li>
		<li <?php if ($mod=="perbandingan") echo('class="active"'); ?> >
			<a href="?mod=perbandingan">Perbandingan</a>
		</li>
	</ul>
	<form class="navbar-form navbar-right" role="search" method="POST" action="?mod=">
		<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-user" aria-hidden="true"></span><?php echo($_SESSION['nama']); ?></button>
		<a href="logout.php"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></button></a>
	</form>
<?php
		break;

	default:
?>
	<ul class="nav navbar-nav">
		<li class="active"><a href="#">Home</a></li>
	</ul>
<?php
}
?>
