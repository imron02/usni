<?php

//error_reporting(0);
session_start();
$username = $_SESSION['username'];
$nama = $_SESSION['nama'];
$role = $_SESSION['role'];
$mod = $_GET['mod'];

//admin
if ($mod == 'persebaran' && $role == 'admin') {
	include 'persebaran.php';
}
elseif ($mod == 'semester' && $role == 'admin') {
	include 'semester.php';
}
elseif ($mod == 'thn_akademik' && $role == 'admin') {
	include 'thn_akademik.php';
}
elseif ($mod == 'fakultas' && $role == 'admin') {
	include 'fakultas.php';
}
elseif ($mod == 'jurusan' && $role == 'admin') {
	include 'jurusan.php';
}
elseif ($mod == 'mahasiswa' && $role == 'admin') {
	include 'mahasiswa.php';
}
elseif ($mod == 'promosi' && $role == 'admin') {
	include 'Promosi.php';
}
elseif ($mod == 'persebaranPromosi' && $role == 'admin') {
	include 'PersebaranPromosi.php';
}
elseif ($mod == 'perbandingan' && $role == 'admin') {
	include 'perbandingan.php';
}
elseif ($role == 'admin') {
?>
<p>
	Hi <strong><?php echo $nama; ?></strong>
	, Selamat Datang di Sistem Persebaran Mahasiswa.
	Silahkan pilih menu pada navigasi di atas.
</p>
<?php
}
else{
	include 'login.php';
}
?>