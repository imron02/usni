<?php
$query = "SELECT m.*, a.nama_fakultas, a.nama_jurusan, s.nama_semester, t.thn FROM
			mahasiswa m
			INNER JOIN(
				SELECT f.*, j.kd_jurusan, j.nama_jurusan FROM
				fakultas f
				INNER JOIN jurusan j
					ON f.kd_fakultas=j.kd_fakultas
			) a ON m.kd_jurusan=a.kd_jurusan
			INNER JOIN semester s
				ON m.kd_semester=s.kd_semester
			INNER JOIN thn_akademik t
				ON m.kd_thn_akademik=t.kd_thn_akademik
			WHERE m.longtitude <> '' AND m.latitude<>'' ";
$thn_dari = $_GET['thn_dari'];
$thn_sampai = $_GET['thn_sampai'];
$nama_jurusan = $_GET['nama_jurusan'];
$nama_fakultas = $_GET['nama_fakultas'];
$nama_semester = $_GET['nama_semester'];
$kota = $_GET['kota'];

if ($thn_dari != '' && $thn_sampai !='') {
	$query.=" AND thn>=$thn_dari AND thn<=$thn_sampai";
}
if ($nama_jurusan !='') {
	$query.=" AND nama_jurusan='$nama_jurusan' ";
}
if ($nama_fakultas !='') {
	$query.=" AND nama_fakultas='$nama_fakultas' ";
}
if ($nama_semester !='') {
	$query.=" AND nama_semester='$nama_semester' ";
}
if ($kota !='') {
	$query.=" AND kota like '%$kota%' ";
}

$result = mysql_query($query);
$rows = mysql_num_rows($result);
$locations = "['USNI',-6.24151,106.783,'usni'],";
//$locations = "";
while ($data = mysql_fetch_assoc($result)) {
	$nm_mhs=$data['nm_mhs'];
	$longtitude=$data['longtitude'];
	$latitude=$data['latitude'];
	$kd_jurusan=$data['kd_jurusan'];
	$locations.="['$nm_mhs',$longtitude,$latitude,'$kd_jurusan'],";
	//$locations.='"formatted_address" : "'.$data["alamat"].', '.$data["kota"].', '.$data["provinsi"].', INA",';
}
?>

<style type='text/css'>
#peta {
width: 100%;
height: 900px;

} </style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

(function() {
	window.onload = function() {
var map;
		//Parameter Google maps
		var options = {
			zoom: 11, //level zoom
		//posisi tengah peta
			center: new google.maps.LatLng(-6.24151,106.783),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

	 // Buat peta di
		var map = new google.maps.Map(document.getElementById('peta'), options);
	 // Tambahkan Marker
		var locations = [
			<?php echo($locations); ?>
		];
		var infowindow = new google.maps.InfoWindow();

		var marker, i;
		 /* kode untuk menampilkan banyak marker */
		for (i = 0; i < locations.length; i++) {
			if (locations[i][3] == "usni") {
				gambar = "usni.png";
			} else {
				gambar = "TI.png";
			}
			// gambar = locations[i][3].concat(".png");
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map,
		 		icon: gambar
			});
		 /* menambahkan event clik untuk menampikan
				infowindows dengan isi sesuai denga
			marker yang di klik */

			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(locations[i][0]);
					infowindow.open(map, marker);
				}
			})(marker, i));
		}
	};
})();
</script>

<form action="index.php" method="GET" enctype="multipart/form-data">
<input type="hidden" name="mod" value="persebaran">
<div class="panel panel-default">
	<div class="panel-heading">FILTER</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Fakultas</button>
					</span>
					<input type="text" class="form-control" placeholder="Fakultas..." name="nama_fakultas" value="<?php echo($_GET['nama_fakultas']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Jurusan</button>
					</span>
					<input type="text" class="form-control" placeholder="Jurusan..." name="nama_jurusan" value="<?php echo($_GET['nama_jurusan']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
		<div class="row">
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Kota</button>
					</span>
					<input type="text" class="form-control" placeholder="Kota..." name="kota" value="<?php echo($_GET['kota']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Semester</button>
					</span>
					<input type="text" class="form-control" placeholder="Semester..." name="semester" value="<?php echo($_GET['nama_semester']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->

		<div class="row">
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Dari</button>
					</span>
					<input type="text" class="form-control" placeholder="Dari tahun..." name="thn_dari" value="<?php echo($_GET['thn_dari']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Sampai</button>
					</span>
					<input type="text" class="form-control" placeholder="Sampai tahun..." name="thn_sampai" value="<?php echo($_GET['thn_sampai']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-primary">Cari</button>
	</div>
</div>
</form>
<div id="peta">
</div>
