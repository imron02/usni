
<?php
$query = "SELECT m.*, a.nama_fakultas, a.nama_jurusan, s.nama_semester, t.thn FROM
			mahasiswa m
			INNER JOIN(
				SELECT f.*, j.kd_jurusan, j.nama_jurusan FROM
				fakultas f
				INNER JOIN jurusan j
					ON f.kd_fakultas=j.kd_fakultas
			) a ON m.kd_jurusan=a.kd_jurusan
			INNER JOIN semester s
				ON m.kd_semester=s.kd_semester
			INNER JOIN thn_akademik t
				ON m.kd_thn_akademik=t.kd_thn_akademik
			WHERE m.nim<>'' ";
$thn_dari = $_GET['thn_dari'];
$thn_sampai = $_GET['thn_sampai'];
$nama_jurusan = $_GET['nama_jurusan'];
$nama_fakultas = $_GET['nama_fakultas'];
$nama_semester = $_GET['nama_semester'];
$kota = $_GET['kota'];

if ($thn_dari != '' && $thn_sampai !='') {
	$query.=" AND thn>=$thn_dari AND thn<=$thn_sampai";
}
if ($nama_jurusan !='') {
	$query.=" AND nama_jurusan='$nama_jurusan' ";
}
if ($nama_fakultas !='') {
	$query.=" AND nama_fakultas='$nama_fakultas' ";
}
if ($nama_semester !='') {
	$query.=" AND nama_semester='$nama_semester' ";
}
if ($kota !='') {
	$query.=" AND kota like '%$kota%' ";
}

$result = mysql_query($query);
$rows = mysql_num_rows($result);
$locations = "['USNI',-6.24151,106.783,'usni'],";
//$locations = "";
while ($data = mysql_fetch_assoc($result)) {
	$nm_mhs=$data['nm_mhs'];
	$longtitude=$data['longtitude'];
	if ($longtitude=="") {
		$longtitude="0";
	}
	$latitude=$data['latitude'];
	if ($latitude=="") {
		$latitude="0";
	}
	$kd_jurusan=$data['kd_jurusan'];
	$alamat=$data['alamat'].' '.$data['kota'].' '.$data['provinsi'].' '.$data['kode_pos'];
	$locations.="['$nm_mhs',$longtitude,$latitude,'$kd_jurusan','$alamat'],";
	//$locations.='"formatted_address" : "'.$data["alamat"].', '.$data["kota"].', '.$data["provinsi"].', INA",';
}
echo($locations);
?>

<style type='text/css'>
#peta {
width: 100%;
height: 900px;
 
} </style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
 
(function() {
	window.onload = function() {
var map;
		//Parameter Google maps
		var options = {
			zoom: 11, //level zoom
		//posisi tengah peta
			center: new google.maps.LatLng(-6.24151,106.783),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
	 
	 // Buat peta di 
		var map = new google.maps.Map(document.getElementById('peta'), options);
	 // Tambahkan Marker 
		var locations = [
			['USNI',-6.24151,106.783,'usni','-6.24151,106.783'],
			['Dedi Saputro',0,0,'TI','Jl.Inpres 08 RT 001/008 Tangerang '],
			['Yohanes Arie Senowibowo',0,0,'TI','Jl.Swakarya bawah no 2 pd Labu Rt 005/009 Jaksel DKI Jakarta '],
			['Quintoaji Kusumo',0,0,'MI','Jl. Danau tondano AA-4 Pejompongan rt 011 rw.006 jakarta DKI jakarta '],
			['Nurmaini',0,0,'SI','Jl.KS Tubun III dalam No 15 A RT 001/004 Jakarta Barat DKI Jakarta '],
			['Mohammad Auliya Ihsani',0,0,'TI','Jl. W2 Slipi palmerah Jakarta Barat DKI Jakarta '],
			['Iriyana',0,0,'TL','jl. peninggaran timur II No. 54 kebayoran lama jakarta selatan DKI Jakarta '],
			['Oka Rizky Bahar',0,0,'SI','JALAN RUSIA IV pondok betung TANGERANG SELATAN BANTEN '],
			['Sugeng Sunarno',0,0,'PS','Jl.Kavling Agraria No.62 RT 002/016 Jakarta TImur DKI Jakarta '],
			['Kurniawan',0,0,'BP','Jalan Madrasyah Rt 006/001 '],
			['Dewi Karyati',0,0,'BP','Pulau Tidung Kepulauan Seribu Kab ADNI DKI JAkarta '],
			['Mega Safitri',0,0,'ME','Jalan Desa Danasari Rt 004/002 '],
			['Ayu Widiastuti',0,0,'AK','Jalan Masjid Al-Mujahidien Gg. Madu Rt 001/004 '],
			['Hilman Silvana',0,0,'ME','Jl.EE Rt 006 Rw.01 Jakarta Barat DKI Jakarta '],
			['Rahma Andayani',0,0,'ME','Jl Ciputat Raya gg Makmur rt 006/003 kebayoran lama Jakarta Selatan DKI Jakarta '],
			['Mala Hayati',0,0,'AK','Jl. Petamburan V Rt.001/08 Jakarta Pusat DKI Jakarta '],
			['Danial Ahmad Subagja',0,0,'HI','Jalan Kayu Manis Utara No. 54 Rt 002/001 '],
			['Saipul Anwar',0,0,'IK','Jalan Cilandak Dalam No. 22 Gang Sawo Rt 004/013 '],
			['Tabita Danun Rombe',0,0,'HI','Jl Cendrawasih Raya No 2A Rt 001 Rw 08 Jakarta Selatan DKI Jakarta '],
			['Dewi Afriani Sanjaya',0,0,'IK','Jl. Impres 8 Larangan Utara Ciledug Banten '],
			['Yesticia Rustwina Silitonga',0,0,'IK','Jl. Banjar Sari XIV cilandak DKI Jakarta ']
		];
		var infowindow = new google.maps.InfoWindow();
		var geocoder = new google.maps.Geocoder();
 
		var marker, i;
		 /* kode untuk menampilkan banyak marker */
		for (i = 0; i < locations.length; i++) {
			gambar = locations[i][3].concat(".png");
			marker = new google.maps.Marker({
				position: new google.maps.Geocoder().geocode( { 'address': locations[i][4]}, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
							results[0].geometry.location;
					} else {
						alert('Geocode was not successful for the following reason: ' + status);
					}
					});
				map: map,
		 		icon: gambar
			});
		 /* menambahkan event clik untuk menampikan
				infowindows dengan isi sesuai denga
			marker yang di klik */
		 
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(locations[i][0]);
					infowindow.open(map, marker);
				}
			})(marker, i));
			alert(i);
		}
	};
}
)();
</script>

<form action="index.php" method="GET" enctype="multipart/form-data">
<input type="hidden" name="mod" value="persebaran">
<div class="panel panel-default">
	<div class="panel-heading">FILTER</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Fakultas</button>
					</span>
					<input type="text" class="form-control" placeholder="Fakultas..." name="nama_fakultas" value="<?php echo($_GET['nama_fakultas']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Jurusan</button>
					</span>
					<input type="text" class="form-control" placeholder="Jurusan..." name="nama_jurusan" value="<?php echo($_GET['nama_jurusan']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
		<div class="row">
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Kota</button>
					</span>
					<input type="text" class="form-control" placeholder="Kota..." name="kota" value="<?php echo($_GET['kota']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Semester</button>
					</span>
					<input type="text" class="form-control" placeholder="Semester..." name="semester" value="<?php echo($_GET['nama_semester']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->

		<div class="row">
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Dari</button>
					</span>
					<input type="text" class="form-control" placeholder="Dari tahun..." name="thn_dari" value="<?php echo($_GET['thn_dari']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Sampai</button>
					</span>
					<input type="text" class="form-control" placeholder="Sampai tahun..." name="thn_sampai" value="<?php echo($_GET['thn_sampai']); ?>">
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-primary">Cari</button>
	</div>
</div>
</form>
<div id="peta">
</div>