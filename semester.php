<?php

switch ($_GET['form']) {
	case 'add':
?>
<form action="?mod=semester&form=insert" method="POST" class="col-lg-7">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>SEMESTER </strong>- Tambah Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Semester</span>
					<input type="text" class="form-control" name="kd_semester" placeholder="kode semester..." aria-describedby="sizing-addon2" maxlength="1">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama Semester</span>
					<input type="text" class="form-control" name="nama_semester" placeholder="nama semester..." aria-describedby="sizing-addon2" maxlength="10">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	case 'insert':
		$kd_semester = $_POST['kd_semester'];
		$nama_semester = $_POST['nama_semester'];
		$query = "INSERT INTO semester (kd_semester,nama_semester) VALUES ('$kd_semester','$nama_semester');";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=semester&kd_semester=$kd_semester&kode=1&do=tambah%20data");
		}
		else{
			header("Location: ?mod=semester&kd_semester=$kd_semester&kode=2&do=tambah%20data");
		}
		break;

	case 'update':
		$kd_semester = $_POST['kd_semester'];
		$nama_semester = $_POST['nama_semester'];
		$query = "UPDATE semester SET nama_semester='$nama_semester' WHERE kd_semester='$kd_semester';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=semester&kd_semester=$kd_semester&kode=1&do=ubah%20data");
		}
		else{
			header("Location: ?mod=semester&kd_semester=$kd_semester&kode=2&do=ubah%20data");
		}
		break;

	case 'delete':
		$kd_semester = $_GET['kd_semester'];
		$query = "DELETE FROM semester WHERE kd_semester='$kd_semester';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=semester&kd_semester=$kd_semester&kode=1&do=hapus%20data");
		}
		else{
			header("Location: ?mod=semester&kd_semester=$kd_semester&kode=2&do=hapus%20data");
		}
		break;

	case 'edit':

		$kd_semester = $_GET['kd_semester'];
		$query = "SELECT * FROM semester WHERE kd_semester='$kd_semester';";
		$result = mysql_query($query);
		$data = mysql_fetch_assoc($result);
?>
<form action="?mod=semester&form=update" method="POST" class="col-lg-7">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>Semester </strong>- Ubah Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Semester</span>
					<input type="text" class="form-control readonly" name="kd_semester" placeholder="kode semester..." aria-describedby="sizing-addon2" maxlength="1" readonly value="<?php echo($data['kd_semester']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama Semester</span>
					<input type="text" class="form-control" name="nama_semester" placeholder="nama semester..." aria-describedby="sizing-addon2" maxlength="10" value="<?php echo($data['nama_semester']); ?>">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	default:
?>
<div class="panel panel-default">
	<div class="panel-heading">SEMESTER</div>
	<div class="panel-body">
		<a href="?mod=semester&form=add">
			<button type="button" class="btn btn-default">+ Tambah Data</button>
		</a>
		<br /><br />
		<table class="table table-bordered" id="example">
			<thead>
				<th>Kode Semester</th>
				<th>Nama Semester</th>
				<th></th>
			</thead>
			<tbody>
<?php
		$query = "SELECT * FROM semester";
		$result = mysql_query($query);
		while ($data = mysql_fetch_assoc($result)) {
			
?>
				<tr>
					<td><?php echo($data['kd_semester']); ?></td>
					<td><?php echo($data['nama_semester']); ?></td>
					<td>
						<a title='Edit'
							href="?mod=semester&form=edit&kd_semester=<?php echo $data['kd_semester']; ?>">
							<img src='img/edit.jpg' width='20'>
						</a>
						<a title='Hapus'
							onclick="return confirm('Anda yakin menghapus data dengan Kode Semester <?php echo $data['kd_semester'] ?> ?')"
							href="?mod=semester&form=delete&kd_semester=<?php echo $data['kd_semester'] ?>">
							<img src='img/delete.jpg' width='20'>
						</a>
					</td>
				</tr>
<?php
		}
?>
			</tbody>
		</table>
	</div>
</div>
<?php
		break;
}

?>