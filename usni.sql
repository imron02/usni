-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2015 at 06:58 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `usni`
--

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE IF NOT EXISTS `fakultas` (
  `kd_fakultas` char(2) NOT NULL,
  `nama_fakultas` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`kd_fakultas`, `nama_fakultas`) VALUES
('EK', 'Ekonomi'),
('FI', 'Ilmu Sosial dan Ilmu Politik'),
('FP', 'Perikanan'),
('TK', 'Teknik');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE IF NOT EXISTS `jurusan` (
  `kd_jurusan` char(2) NOT NULL,
  `kd_fakultas` char(2) NOT NULL,
  `nama_jurusan` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`kd_jurusan`, `kd_fakultas`, `nama_jurusan`) VALUES
('AK', 'EK', 'Akutansi'),
('BP', 'FP', 'Budidaya Perairan'),
('HI', 'FI', 'Hubungan Internasional'),
('IK', 'FI', 'Ilmu Komunikasi'),
('ME', 'EK', 'Manajemen'),
('MI', 'TK', 'Manajemen Informatika D3'),
('PS', 'FP', 'Pemanfaatan Sumber Daya Perikanan'),
('SI', 'TK', 'Sistem Informasi'),
('TI', 'TK', 'Teknik Informatika'),
('TL', 'TK', 'Teknik Lingkungan');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `nim` char(20) NOT NULL DEFAULT '',
  `nm_mhs` char(30) DEFAULT NULL,
  `kd_thn_akademik` char(4) DEFAULT NULL,
  `tempat_lahir` char(30) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `kd_jurusan` char(2) DEFAULT NULL,
  `kd_semester` char(1) DEFAULT NULL,
  `alamat` char(255) DEFAULT NULL,
  `kota` char(20) DEFAULT NULL,
  `provinsi` char(20) DEFAULT NULL,
  `kd_pos` char(7) DEFAULT NULL,
  `longtitude` varchar(30) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `no_tlp` char(50) DEFAULT NULL,
  `no_hp` char(50) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `password` char(8) DEFAULT NULL,
  `tgl_input` datetime DEFAULT CURRENT_TIMESTAMP,
  `titik_mhs` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nm_mhs`, `kd_thn_akademik`, `tempat_lahir`, `tgl_lahir`, `kd_jurusan`, `kd_semester`, `alamat`, `kota`, `provinsi`, `kd_pos`, `longtitude`, `latitude`, `no_tlp`, `no_hp`, `email`, `password`, `tgl_input`, `titik_mhs`) VALUES
('011201503125131', 'Dedi Saputro', '12', 'Wonogiri', '0000-00-00', 'TI', '2', 'Jl.Inpres 08 RT 001/008', '', 'Tangerang', '15154', '-6.2425783', '106.7489103', '', '085776205446', '', '', '2015-02-03 15:42:41', 'mhs'),
('011201573125007', 'Yohanes Arie Senowibowo', '12', 'Jakarta', '0000-00-00', 'TI', '2', 'Jl.Swakarya bawah no 2 pd Labu Rt 005/009', 'Jaksel', 'DKI Jakarta', '12450', '-6.3054775', '106.7986405', '02189663123', '', '', '', '2015-02-03 15:42:41', 'mhs'),
('011203303125010', 'Quintoaji Kusumo', '12', 'Semarang', '0000-00-00', 'MI', '2', 'Jl. Danau tondano AA-4 Pejompongan rt 011 rw.006', 'jakarta', 'DKI jakarta', '10210', '-6.2109715', '106.827749', '91508979', '081219513772', '', '', '2015-02-03 15:42:41', 'mhs'),
('011205503125038', 'Nurmaini', '12', 'Jakarta', '0000-00-00', 'SI', '2', 'Jl.KS Tubun III dalam No 15 A RT 001/004', 'Jakarta Barat', 'DKI Jakarta', '11410', '-6.1882052', '106.8079949', '', '081318648959', '', '', '2015-02-03 15:42:41', 'mhs'),
('011301503125170', 'Mohammad Auliya Ihsani', '13', 'Jakarta', '0000-00-00', 'TI', '2', 'Jl. W2 Slipi palmerah', 'Jakarta Barat', 'DKI Jakarta', '', '-6.1923809', '106.8004882', '', '083875928684', '', '', '2015-02-03 15:42:41', 'mhs'),
('011302573125020', 'Iriyana', '13', 'Jakarta', '0000-00-00', 'TL', '2', 'jl. peninggaran timur II No. 54 kebayoran lama', 'jakarta selatan', 'DKI Jakarta', '12240', '-6.244463', '106.7761159', '0217294129', '083873964092', '', '', '2015-02-03 15:42:41', 'mhs'),
('011305503125063', 'Oka Rizky Bahar', '13', 'Jakarta', '0000-00-00', 'SI', '2', 'JALAN RUSIA IV pondok betung', 'TANGERANG SELATAN', 'BANTEN', '15221', '', '106.7327668', '', '', '', '', '2015-02-03 15:42:41', 'mhs'),
('021201503125020', 'Sugeng Sunarno', '12', 'Purbalingga', '0000-00-00', 'PS', '2', 'Jl.Kavling Agraria No.62 RT 002/016', 'Jakarta TImur', 'DKI Jakarta', '13440', '-6.2472417', '106.9123539', '', '089646384937', '', '', '2015-02-03 15:42:41', 'mhs'),
('021202503125001', 'Kurniawan', '12', 'Jakarta', '1994-04-18', 'BP', '1', 'Jalan Madrasyah Rt 006/001', '', '', '', '-6.3670463', '106.8872425', '08567094241', '', '', '', '2015-02-03 15:00:09', 'mhs'),
('021302503125006', 'Dewi Karyati', '13', 'Jakarta', '0000-00-00', 'BP', '1', 'Pulau Tidung Kepulauan Seribu Kab ADNI', '', 'DKI JAkarta', '', '-5.7985251', '106.5071982', '', '085776205446', '', '', '2015-02-03 15:42:41', 'mhs'),
('041201503125001', 'Mega Safitri', '12', 'Tegal', '1989-04-29', 'ME', '1', 'Jalan Desa Danasari Rt 004/002', '', '', '', '-6.188246', '106.792957', '085711448236', '', '', '', '2015-02-03 15:00:09', 'mhs'),
('041202503125001', 'Ayu Widiastuti', '12', 'Jakarta', '1994-11-26', 'AK', '1', 'Jalan Masjid Al-Mujahidien Gg. Madu Rt 001/004', '', '', '', '-6.277855', '106.700995', '021-91520582', '083897510021', '', '', '2015-02-03 15:00:09', 'mhs'),
('041301503125116', 'Hilman Silvana', '13', 'Jakarta', '0000-00-00', 'ME', '1', 'Jl.EE Rt 006 Rw.01', 'Jakarta Barat', 'DKI Jakarta', '11540', '-6.1516143', '106.7252709', '91508979', '081219513772', '', '', '2015-02-03 15:42:41', 'mhs'),
('041301503125202', 'Rahma Andayani', '13', 'Jakarta', '0000-00-00', 'ME', '2', 'Jl Ciputat Raya gg Makmur rt 006/003 kebayoran lama', 'Jakarta Selatan', 'DKI Jakarta', '12240', '-6.2812452', '106.7712222', '', '087881396591', '', '', '2015-02-03 15:42:41', 'mhs'),
('041302503125086', 'Mala Hayati', '13', 'Jakarta', '0000-00-00', 'AK', '1', 'Jl. Petamburan V Rt.001/08', 'Jakarta Pusat', 'DKI Jakarta', '10260', '-6.1977402', '106.8073004', '02189663123', '', '', '', '2015-02-03 15:42:41', 'mhs'),
('051201503125001', 'Danial Ahmad Subagja', '12', 'Jakarta', '1992-12-11', 'HI', '1', 'Jalan Kayu Manis Utara No. 54 Rt 002/001', '', '', '', '-6.1961936', '106.8603969', '087824687240', '', '', '', '2015-02-03 15:00:09', 'mhs'),
('051203503125001', 'Saipul Anwar', '12', 'Jakarta', '1993-08-04', 'IK', '1', 'Jalan Cilandak Dalam No. 22 Gang Sawo Rt 004/013', '', '', '', '-6.2871922', '106.8033655', '7512461', '089636370341', '', '', '2015-02-03 15:00:09', 'mhs'),
('051301503125017', 'Tabita Danun Rombe', '13', 'Jakarta', '0000-00-00', 'HI', '1', 'Jl Cendrawasih Raya No 2A Rt 001 Rw 08', 'Jakarta Selatan', 'DKI Jakarta', '12240', '-6.253665', '106.7572976', '', '081318648959', '', '', '2015-02-03 15:42:41', 'mhs'),
('051303503125010', 'Dewi Afriani Sanjaya', '13', 'Tangerang', '0000-00-00', 'IK', '1', 'Jl. Impres 8 Larangan Utara Ciledug', '', 'Banten', '15154', '-6.2323129', '', '', '089646384937', '', '', '2015-02-03 15:42:41', 'mhs'),
('051303503125175', 'Yesticia Rustwina Silitonga', '13', 'Jakarta', '0000-00-00', 'IK', '2', 'Jl. Banjar Sari XIV cilandak', '', 'DKI Jakarta', '12430', '-6.2915227', '106.7934261', '02175900731', '089619223671', '', '', '2015-02-03 15:42:41', 'mhs'),
('89295', 'wowo', '10', 'depok', '2015-03-10', 'HI', '1', 'depok', 'depok', 'jawa barat', '23', '-6.1961936', '106.8603969', '865', '968', 'wowo@wowo.com', '', '2015-03-23 14:28:10', 'mhs');

-- --------------------------------------------------------

--
-- Table structure for table `promosi`
--

CREATE TABLE IF NOT EXISTS `promosi` (
`id_promosi` int(11) NOT NULL,
  `kd_thn_akademik` char(4) NOT NULL,
  `kd_semester` char(1) NOT NULL,
  `nama_tempat` varchar(255) NOT NULL,
  `alamat` char(255) NOT NULL,
  `kota` char(20) NOT NULL,
  `provinsi` char(20) NOT NULL,
  `kd_poss` char(7) NOT NULL,
  `longtitude` varchar(30) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `tgl_input` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `titik_promosi` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `promosi`
--

INSERT INTO `promosi` (`id_promosi`, `kd_thn_akademik`, `kd_semester`, `nama_tempat`, `alamat`, `kota`, `provinsi`, `kd_poss`, `longtitude`, `latitude`, `tgl_input`, `titik_promosi`) VALUES
(7, '09', '2', 'Kuningan', 'Kuningan', 'kuningan', 'Jakarta', '12345', '-6.2425783', '106.7489103 	', '2015-03-02 15:37:11', 'promosi'),
(8, '10', '2', 'DKI Jakarta', 'DKI Jakarta', 'Jakarta', 'Jakarta', '12345', '-6.1516143', '106.7252709', '2015-03-02 15:54:36', 'promosi'),
(9, '14', '2', 'bogor', 'bogor', 'bogor', 'jawa barat', '1231', '-6.2915227', '106.7934261', '2015-03-23 11:49:47', 'promosi');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE IF NOT EXISTS `semester` (
  `kd_semester` char(1) DEFAULT NULL,
  `nama_semester` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`kd_semester`, `nama_semester`) VALUES
('2', 'Genap'),
('1', 'Ganjil');

-- --------------------------------------------------------

--
-- Table structure for table `thn_akademik`
--

CREATE TABLE IF NOT EXISTS `thn_akademik` (
  `kd_thn_akademik` char(2) DEFAULT NULL,
  `thn` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `thn_akademik`
--

INSERT INTO `thn_akademik` (`kd_thn_akademik`, `thn`) VALUES
('09', 2009),
('10', 2010),
('11', 2011),
('12', 2012),
('13', 2013),
('14', 2014),
('15', 2015),
('16', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` char(20) NOT NULL,
  `password` char(8) NOT NULL,
  `nama` char(30) NOT NULL,
  `role` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `nama`, `role`) VALUES
('admin', 'admin', 'USNI', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
 ADD PRIMARY KEY (`kd_fakultas`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
 ADD PRIMARY KEY (`kd_jurusan`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
 ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `promosi`
--
ALTER TABLE `promosi`
 ADD PRIMARY KEY (`id_promosi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `promosi`
--
ALTER TABLE `promosi`
MODIFY `id_promosi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
