<?php

switch ($_GET['form']) {
	case 'add':
?>
<form action="?mod=mahasiswa&form=insert" method="POST" class="col-lg-8">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>MAHASISWA </strong>- Tambah Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">NIM</span>
					<input type="text" class="form-control" name="nim" placeholder="nomor induk mahasiswa..." aria-describedby="sizing-addon2" maxlength="8">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama</span>
					<input type="text" class="form-control" name="nm_mhs" placeholder="nama mahasiswa..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tahun</span>
					<select name="kd_thn_akademik" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
<?php
		$query_thn_akademik = "SELECT * FROM thn_akademik";
		$result_thn_akademik = mysql_query($query_thn_akademik);
		while ($data_thn_akademik =  mysql_fetch_assoc($result_thn_akademik)) {
?>
						<option value="<?php echo($data_thn_akademik['kd_thn_akademik']); ?>"><?php echo($data_thn_akademik['thn']); ?></option>
<?php
		}
?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Semester</span>
					<select name="kd_semester" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
<?php
		$query_semester = "SELECT * FROM semester";
		$result_semester = mysql_query($query_semester);
		while ($data_semester =  mysql_fetch_assoc($result_semester)) {
?>
						<option value="<?php echo($data_semester['kd_semester']); ?>"><?php echo($data_semester['nama_semester']); ?></option>
<?php
		}
?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Fakultas - Jurusan</span>
					<select name="kd_jurusan" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
<?php
		$query_fakultas_jurusan = "SELECT f.*, j.kd_jurusan, j.nama_jurusan
							FROM fakultas f
							LEFT OUTER JOIN jurusan j ON f.kd_fakultas=j.kd_fakultas
							ORDER BY nama_fakultas;";
		$result_fakultas_jurusan = mysql_query($query_fakultas_jurusan);
		while ($data_fakultas_jurusan =  mysql_fetch_assoc($result_fakultas_jurusan)) {
?>
						<option value="<?php echo($data_fakultas_jurusan['kd_jurusan']); ?>"><?php echo($data_fakultas_jurusan['nama_fakultas'] . ' - ' . $data_fakultas_jurusan['nama_jurusan']); ?></option>
<?php
		}
?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tempat Lahir</span>
					<input type="text" class="form-control" name="tempat_lahir" placeholder="tempat lahir..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tanggal Lahir</span>
					<input type="text" class="form-control" name="tgl_lahir" placeholder="tanggal lahir format yyyy-MM-dd..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Alamat</span>
					<textarea class="form-control" name="alamat" placeholder="alamat..." ></textarea>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kota</span>
					<input type="text" class="form-control" name="kota" placeholder="kota..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Provinsi</span>
					<input type="text" class="form-control" name="provinsi" placeholder="provinsi..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Pos</span>
					<input type="text" class="form-control" name="kd_pos" placeholder="kode pos..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">No Telepon</span>
					<input type="text" class="form-control" name="no_tlp" placeholder="no telp..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">No HP</span>
					<input type="text" class="form-control" name="no_hp" placeholder="no hp..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">email</span>
					<input type="text" class="form-control" name="email" placeholder="email..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Longtitude</span>
					<input type="text" class="form-control" name="longtitude" placeholder="longtitude..." aria-describedby="sizing-addon2">
				</div>
				<br/>
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Latitude</span>
					<input type="text" class="form-control" name="latitude" placeholder="latitude..." aria-describedby="sizing-addon2">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	case 'edit':
		$nim = $_GET['nim'];
		$query = "SELECT m.*, a.nama_fakultas, a.nama_jurusan, s.nama_semester, t.thn
					FROM mahasiswa m
					LEFT OUTER JOIN(
						SELECT f.*, j.kd_jurusan, j.nama_jurusan
							FROM fakultas f
							LEFT OUTER JOIN jurusan j ON f.kd_fakultas=j.kd_fakultas
					) a ON m.kd_jurusan=a.kd_jurusan
					LEFT OUTER JOIN semester s ON m.kd_semester=s.kd_semester
					LEFT OUTER JOIN thn_akademik t ON m.kd_thn_akademik=t.kd_thn_akademik
					WHERE m.nim='$nim';";
		$result = mysql_query($query);
		$data = mysql_fetch_assoc($result);
?>
<form action="?mod=mahasiswa&form=update" method="POST" class="col-lg-8">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>MAHASISWA </strong>- Edit Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">NIM</span>
					<input type="text" class="form-control readonly" name="nim" placeholder="nomor induk mahasiswa..." aria-describedby="sizing-addon2" maxlength="8" value="<?php echo($data['nim']); ?>" readonly>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama</span>
					<input type="text" class="form-control" name="nm_mhs" placeholder="nama mahasiswa..." aria-describedby="sizing-addon2" value="<?php echo($data['nm_mhs']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tahun</span>
					<select name="kd_thn_akademik" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
<?php
		$query_thn_akademik = "SELECT * FROM thn_akademik";
		$result_thn_akademik = mysql_query($query_thn_akademik);
		while ($data_thn_akademik =  mysql_fetch_assoc($result_thn_akademik)) {
?>
						<option value="<?php echo($data_thn_akademik['kd_thn_akademik']); ?>" <?php if($data_thn_akademik['kd_thn_akademik']==$data['kd_thn_akademik']) echo("SELECTED"); ?>><?php echo($data_thn_akademik['thn']); ?></option>
<?php
		}
?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Semester</span>
					<select name="kd_semester" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
<?php
		$query_semester = "SELECT * FROM semester";
		$result_semester = mysql_query($query_semester);
		while ($data_semester =  mysql_fetch_assoc($result_semester)) {
?>
						<option value="<?php echo($data_semester['kd_semester']); ?>" <?php if($data_semester['kd_semester']==$data['kd_semester']) echo('SELECTED') ?>><?php echo($data_semester['nama_semester']); ?></option>
<?php
		}
?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Fakultas - Jurusan</span>
					<select name="kd_jurusan" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
<?php
		$query_fakultas_jurusan = "SELECT f.*, j.kd_jurusan, j.nama_jurusan
							FROM fakultas f
							LEFT OUTER JOIN jurusan j ON f.kd_fakultas=j.kd_fakultas
							ORDER BY nama_fakultas;";
		$result_fakultas_jurusan = mysql_query($query_fakultas_jurusan);
		while ($data_fakultas_jurusan =  mysql_fetch_assoc($result_fakultas_jurusan)) {
?>
						<option value="<?php echo($data_fakultas_jurusan['kd_jurusan']); ?>" <?php if($data_fakultas_jurusan['kd_jurusan']==$data['kd_jurusan']) echo("SELECTED"); ?>><?php echo($data_fakultas_jurusan['nama_fakultas'] . ' - ' . $data_fakultas_jurusan['nama_jurusan']); ?></option>
<?php
		}
?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tempat Lahir</span>
					<input type="text" class="form-control" name="tempat_lahir" placeholder="tempat lahir..." aria-describedby="sizing-addon2" value="<?php echo($data['tempat_lahir']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tanggal Lahir</span>
					<input type="text" class="form-control" name="tgl_lahir" placeholder="tanggal lahir format yyyy-MM-dd..." aria-describedby="sizing-addon2" value="<?php echo($data['tgl_lahir']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Alamat</span>
					<textarea class="form-control" name="alamat" placeholder="alamat..." ><?php echo($data['alamat']); ?></textarea>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kota</span>
					<input type="text" class="form-control" name="kota" placeholder="kota..." aria-describedby="sizing-addon2"  value="<?php echo($data['kota']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Provinsi</span>
					<input type="text" class="form-control" name="provinsi" placeholder="provinsi..." aria-describedby="sizing-addon2"  value="<?php echo($data['provinsi']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Pos</span>
					<input type="text" class="form-control" name="kd_pos" placeholder="kode pos..." aria-describedby="sizing-addon2"  value="<?php echo($data['kd_pos']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">No Telepon</span>
					<input type="text" class="form-control" name="no_tlp" placeholder="no telp..." aria-describedby="sizing-addon2" value="<?php echo($data['no_tlp']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">No HP</span>
					<input type="text" class="form-control" name="no_hp" placeholder="no hp..." aria-describedby="sizing-addon2" value="<?php echo($data['no_hp']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">email</span>
					<input type="text" class="form-control" name="email" placeholder="email..." aria-describedby="sizing-addon2" value="<?php echo($data['email']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Longtitude</span>
					<input type="text" class="form-control" name="longtitude" placeholder="longtitude..." aria-describedby="sizing-addon2" value="<?php echo($data['longtitude']); ?>">
				</div>
				<br/>
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Latitude</span>
					<input type="text" class="form-control" name="latitude" placeholder="latitude..." aria-describedby="sizing-addon2" value="<?php echo($data['latitude']); ?>">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	case 'insert':
		$nim = $_POST['nim'];
		$nm_mhs = $_POST['nm_mhs'];
		$kd_thn_akademik = $_POST['kd_thn_akademik'];
		$kd_jurusan = $_POST['kd_jurusan'];
		$kd_semester = $_POST['kd_semester'];
		$tempat_lahir = $_POST['tempat_lahir'];
		$tgl_lahir = $_POST['tgl_lahir'];
		$alamat = $_POST['alamat'];
		$kota = $_POST['kota'];
		$provinsi = $_POST['provinsi'];
		$kd_pos = $_POST['kd_pos'];
		$no_tlp = $_POST['no_tlp'];
		$no_hp = $_POST['no_hp'];
		$email = $_POST['email'];
		$longtitude = $_POST['longtitude'];
		$latitude = $_POST['latitude'];

		$query = "INSERT INTO mahasiswa
						(nim,nm_mhs,kd_thn_akademik,
						tempat_lahir,tgl_lahir,kd_jurusan,
						kd_semester,alamat,kota,provinsi,kd_pos,
						longtitude,latitude,no_tlp,no_hp,
						email,password,tgl_input,titik_mhs)
					VALUES
					('$nim','$nm_mhs','$kd_thn_akademik',
						'$tempat_lahir','$tgl_lahir','$kd_jurusan',
						'$kd_semester','$alamat','$kota','$provinsi','$kd_pos',
						'$longtitude','$latitude','$no_tlp','$no_hp',
						'$email','',NOW(),'mhs');";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=mahasiswa&nim=$nim&kode=1&do=tambah%20data");
			//echo $query;
		}
		else{
			header("Location: ?mod=mahasiswa&nim=$nim&kode=2&do=tambah%20data");
			//echo $query;
		}

		break;

	case 'update':
		$nim = $_POST['nim'];
		$nm_mhs = $_POST['nm_mhs'];
		$kd_thn_akademik = $_POST['kd_thn_akademik'];
		$kd_jurusan = $_POST['kd_jurusan'];
		$kd_semester = $_POST['kd_semester'];
		$tempat_lahir = $_POST['tempat_lahir'];
		$tgl_lahir = $_POST['tgl_lahir'];
		$alamat = $_POST['alamat'];
		$kota = $_POST['kota'];
		$provinsi = $_POST['provinsi'];
		$kd_pos = $_POST['kd_pos'];
		$no_tlp = $_POST['no_tlp'];
		$no_hp = $_POST['no_hp'];
		$email = $_POST['email'];
		$longtitude = $_POST['longtitude'];
		$latitude = $_POST['latitude'];

		$query = "UPDATE mahasiswa SET
						nim='$nim',nm_mhs='$nm_mhs',kd_thn_akademik='$kd_thn_akademik',
						tempat_lahir='$tempat_lahir',tgl_lahir='$tgl_lahir',kd_jurusan='$kd_jurusan',
						kd_semester='$kd_semester',alamat='$alamat',kota='$kota',provinsi='$provinsi',kd_pos='$kd_pos',
						longtitude='$longtitude',latitude='$latitude',no_tlp='$no_tlp',no_hp='$no_hp',
						email='$email',password='$password'
					WHERE nim='$nim';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=mahasiswa&nim=$nim&kode=1&do=edit%20data");
		}
		else{
			header("Location: ?mod=mahasiswa&nim=$nim&kode=2&do=edit%20data");
		}
		break;

	case 'delete':
		$nim = $_GET['nim'];
		$query = "DELETE FROM mahasiswa WHERE nim='$nim';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=mahasiswa&nim=$nim&kode=1&do=edit%20data");
			//echo $query;
		}
		else{
			header("Location: ?mod=mahasiswa&nim=$nim&kode=2&do=edit%20data");
			//echo $query;
		}
		break;

	default:
?>
<div class="panel panel-default">
	<div class="panel-heading">MAHASISWA</div>
	<div class="panel-body">
		<a href="?mod=mahasiswa&form=add">
			<button type="button" class="btn btn-default">+ Tambah Data</button>
		</a>
		<br /><br />
		<div class="table-responsive">
			<table class="table table-bordered lebihkecil" id="example" width="2500">
				<thead>
					<th class="tengah">NIM</th>
					<th class="tengah">Nama</th>
					<th class="tengah">Tahun</th>
					<th class="tengah">Semester</th>
					<th class="tengah">Fakultas</th>
					<th class="tengah">Jurusan</th>
					<th class="tengah">Tpt. Lahir</th>
					<th class="tengah">Tgl. Lahir</th>
					<th class="tengah">Alamat</th>
					<th class="tengah">Kota</th>
					<th class="tengah">Provinsi</th>
					<th class="tengah">KodePos</th>
					<th class="tengah">No. Telp</th>
					<th class="tengah">No. HP</th>
					<th class="tengah">email</th>
					<th class="tengah">Longtitude</th>
					<th class="tengah">Latitude</th>
					<th class="tengah"></th>
				</thead>
				<tbody>
	<?php
			$query = "SELECT m.*, a.nama_fakultas, a.nama_jurusan, s.nama_semester, t.thn
						FROM mahasiswa m
						LEFT OUTER JOIN(
							SELECT f.*, j.kd_jurusan, j.nama_jurusan
								FROM fakultas f
								LEFT OUTER JOIN jurusan j ON f.kd_fakultas=j.kd_fakultas
						) a ON m.kd_jurusan=a.kd_jurusan
						LEFT OUTER JOIN semester s ON m.kd_semester=s.kd_semester
						LEFT OUTER JOIN thn_akademik t ON m.kd_thn_akademik=t.kd_thn_akademik;";
			$result = mysql_query($query);
			while ($data = mysql_fetch_assoc($result)) {

	?>
					<tr>
						<td><?php echo($data['nim']); ?></td>
						<td><?php echo($data['nm_mhs']); ?></td>
						<td><?php echo($data['thn']); ?></td>
						<td><?php echo($data['nama_semester']); ?></td>
						<td><?php echo($data['nama_fakultas']); ?></td>
						<td><?php echo($data['nama_jurusan']); ?></td>
						<td><?php echo($data['tempat_lahir']); ?></td>
						<td><?php echo($data['tgl_lahir']); ?></td>
						<td><?php echo($data['alamat']); ?></td>
						<td><?php echo($data['kota']); ?></td>
						<td><?php echo($data['provinsi']); ?></td>
						<td><?php echo($data['kd_pos']); ?></td>
						<td><?php echo($data['no_tlp']); ?></td>
						<td><?php echo($data['no_hp']); ?></td>
						<td><?php echo($data['email']); ?></td>
						<td><?php echo($data['longtitude']); ?></td>
						<td><?php echo($data['latitude']); ?></td>
						<td>
							<a title='Edit'
								href="?mod=mahasiswa&form=edit&nim=<?php echo $data['nim']; ?>">
								<img src='img/edit.jpg' width='20'>
							</a>
							<a title='Hapus'
								onclick="return confirm('Anda yakin menghapus data dengan NIM <?php echo $data['nim']; ?> ?')"
								href="?mod=mahasiswa&form=delete&nim=<?php echo ($data['nim']); ?>">
								<img src='img/delete.jpg' width='20'>
							</a>
						</td>
					</tr>
	<?php
			}
	?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
}

?>
