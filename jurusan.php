<?php

switch ($_GET['form']) {
	case 'add':
?>
<form action="?mod=jurusan&form=insert" method="POST" class="col-lg-7">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>JURUSAN </strong>- Tambah Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Jurusan</span>
					<input type="text" class="form-control" name="kd_jurusan" placeholder="kode jurusan..." aria-describedby="sizing-addon2" maxlength="2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama Jurusan</span>
					<input type="text" class="form-control" name="nama_jurusan" placeholder="nama jurusan..." aria-describedby="sizing-addon2" maxlength="50" >
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Fakultas</span>
					<select class="form-control" name="kd_fakultas">
						<option value=""></option>
<?php
		$query_fakultas = "SELECT * FROM fakultas";
		$result_fakultas = mysql_query($query_fakultas);
		while ($data_fakultas = mysql_fetch_assoc($result_fakultas)) {
?>
						<option value="<?php echo($data_fakultas['kd_fakultas']); ?>"><?php echo($data_fakultas['kd_fakultas'] . ' - ' .$data_fakultas['nama_fakultas'] ) ?></option>
<?php
		}
?>
					</select>
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	case 'edit':

		$kd_jurusan = $_GET['kd_jurusan'];
		$query = "SELECT a.*,b.nama_fakultas FROM jurusan a LEFT OUTER JOIN fakultas b ON a.kd_fakultas=b.kd_fakultas WHERE kd_jurusan='$kd_jurusan';";
		$result = mysql_query($query);
		$data = mysql_fetch_assoc($result);
?>
<form action="?mod=jurusan&form=update" method="POST" class="col-lg-7">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>JURUSAN </strong>- Edit Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Jurusan</span>
					<input type="text" class="form-control readonly" name="kd_jurusan" placeholder="kode jurusan..." aria-describedby="sizing-addon2" maxlength="2" readonly value="<?php echo($data['kd_jurusan']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama Jurusan</span>
					<input type="text" class="form-control" name="nama_jurusan" placeholder="nama jurusan..." aria-describedby="sizing-addon2" maxlength="50" value="<?php echo($data['nama_jurusan']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Fakultas</span>
					<select class="form-control" name="kd_fakultas">
<?php
		$query_fakultas = "SELECT * FROM fakultas";
		$result_fakultas = mysql_query($query_fakultas);
		while ($data_fakultas = mysql_fetch_assoc($result_fakultas)) {
?>
						<option value="<?php echo($data_fakultas['kd_fakultas']); ?>" <?php if ($data_fakultas['kd_fakultas']==$data['kd_fakultas']) echo('SELECTED'); ?>><?php echo($data_fakultas['kd_fakultas'] . ' - ' .$data_fakultas['nama_fakultas'] ) ?></option>
<?php
		}
?>
					</select>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	case 'insert':
		$kd_jurusan = $_POST['kd_jurusan'];
		$nama_jurusan = $_POST['nama_jurusan'];
		$kd_fakultas = $_POST['kd_fakultas'];
		$query = "INSERT INTO jurusan (kd_jurusan,nama_jurusan,kd_fakultas) VALUES ('$kd_jurusan','$nama_jurusan','$kd_fakultas');";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=jurusan&kd_jurusan=$kd_jurusan&kode=1&do=tambah%20data");
		}
		else{
			header("Location: ?mod=jurusan&kd_jurusan=$kd_jurusan&kode=2&do=tambah%20data");
		}
		break;

	case 'update':
		$kd_jurusan = $_POST['kd_jurusan'];
		$nama_jurusan = $_POST['nama_jurusan'];
		$kd_fakultas = $_POST['kd_fakultas'];
		$query = "UPDATE jurusan SET nama_jurusan='$nama_jurusan', kd_fakultas='$kd_fakultas' WHERE kd_jurusan='$kd_jurusan';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=jurusan&kd_fakultas=$kd_jurusan&kode=1&do=ubah%20data");
		}
		else{
			header("Location: ?mod=jurusan&kd_jurusan=$kd_jurusan&kode=2&do=ubah%20data");
		}
		break;

	case 'delete':
		$kd_jurusan = $_GET['kd_jurusan'];
		$query = "DELETE FROM jurusan WHERE kd_jurusan='$kd_jurusan';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=jurusan&kd_jurusan=$kd_jurusan&kode=1&do=hapus%20data");
		}
		else{
			header("Location: ?mod=jurusan&kd_jurusan=$kd_jurusan&kode=2&do=hapus%20data");
		}
		break;
	
	default:
?>
<div class="panel panel-default">
	<div class="panel-heading">JURUSAN</div>
	<div class="panel-body">
		<a href="?mod=jurusan&form=add">
			<button type="button" class="btn btn-default">+ Tambah Data</button>
		</a>
		<br /><br />
		<table class="table table-bordered" id="example">
			<thead>
				<th>Kode Jurusan</th>
				<th>Nama Jurusan</th>
				<th>Kode Fakultas</th>
				<th>Nama Fakultas</th>
				<th></th>
			</thead>
			<tbody>
<?php
		$query = "SELECT a.*,b.nama_fakultas FROM jurusan a LEFT OUTER JOIN fakultas b ON a.kd_fakultas=b.kd_fakultas;";
		$result = mysql_query($query);
		while ($data = mysql_fetch_assoc($result)) {
			
?>
				<tr>
					<td><?php echo($data['kd_jurusan']); ?></td>
					<td><?php echo($data['nama_jurusan']); ?></td>
					<td><?php echo($data['kd_fakultas']); ?></td>
					<td><?php echo($data['nama_fakultas']); ?></td>
					<td>
						<a title='Edit'
							href="?mod=jurusan&form=edit&kd_jurusan=<?php echo $data['kd_jurusan']; ?>">
							<img src='img/edit.jpg' width='20'>
						</a>
						<a title='Hapus'
							onclick="return confirm('Anda yakin menghapus data dengan Kode Jurusan <?php echo $data['kd_jurusan'] ?> ?')"
							href="?mod=jurusan&form=delete&kd_jurusan=<?php echo $data['kd_jurusan'] ?>">
							<img src='img/delete.jpg' width='20'>
						</a>
					</td>
				</tr>
<?php
		}
?>
			</tbody>
		</table>
	</div>
</div>
<?php
		break;
}
?>