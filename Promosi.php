<?php

switch ($_GET['form']) {
	case 'add':
?>
<form action="?mod=promosi&form=insert" method="POST" class="col-lg-8">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>PROMOSI </strong>- Tambah Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">ID PROMOSI</span>
					<input type="text" class="form-control" name="id" placeholder="ID Promosi..." aria-describedby="sizing-addon2" maxlength="1">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tahun</span>
					<select name="kd_thn_akademik" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
<?php
		$query_thn_akademik = "SELECT * FROM thn_akademik";
		$result_thn_akademik = mysql_query($query_thn_akademik);
		while ($data_thn_akademik =  mysql_fetch_assoc($result_thn_akademik)) {
?>
						<option value="<?php echo($data_thn_akademik['kd_thn_akademik']); ?>"><?php echo($data_thn_akademik['thn']); ?></option>
<?php
		}
?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Semester</span>
					<select name="kd_semester" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
<?php
		$query_semester = "SELECT * FROM semester";
		$result_semester = mysql_query($query_semester);
		while ($data_semester =  mysql_fetch_assoc($result_semester)) {
?>
						<option value="<?php echo($data_semester['kd_semester']); ?>"><?php echo($data_semester['nama_semester']); ?></option>
<?php
		}
?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama Tempat</span>
					<input type="text" class="form-control" name="nama_tempat" placeholder="Nama Tempat..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Alamat</span>
					<textarea class="form-control" name="alamat" placeholder="Alamat..." aria-describedby="sizing-addon2"> </textarea>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kota</span>
					<input type="text" name="kota" class="form-control" placeholder="Kota..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Provinsi</span>
					<input type="text" class="form-control" name="provinsi" placeholder="tempat lahir..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Pos</span>
					<input type="text" class="form-control" name="kd_poss" placeholder="Kode Pos..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Longtitude</span>
					<input type="text" class="form-control" name="longtitude" placeholder="Longtitude..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Latitude</span>
					<input type="text" class="form-control" name="latitude" placeholder="Latitude..." aria-describedby="sizing-addon2">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	case 'edit':
		$id_promosi = $_GET['id_promosi'];
		$query = "select * from promosi	WHERE id_promosi='$id_promosi'";
		$result = mysql_query($query);
		$data = mysql_fetch_assoc($result);
?>
<form action="?mod=promosi&form=update&id_promosi=<?php echo $id_promosi;?>" method="POST" class="col-lg-8">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>PROMOSI </strong>- Edit Data</div>
		<div class="panel-body">
			<table class="form">
			<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">ID PROMOSI</span>
					<input type="text" class="form-control" name="id_promosi" placeholder="ID Promosi..." aria-describedby="sizing-addon2" maxlength="1" value="<?php echo($data['id_promosi']); ?>" readonly>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tahun</span>
					<select name="kd_thn_akademik" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
<?php
		$query_thn_akademik = "SELECT * FROM thn_akademik";
		$result_thn_akademik = mysql_query($query_thn_akademik);
		while ($data_thn_akademik =  mysql_fetch_assoc($result_thn_akademik)) {
?>
						<option value="<?php echo($data_thn_akademik['kd_thn_akademik']); ?>" <?php if($data_thn_akademik['kd_thn_akademik']==$data['kd_thn_akademik']) echo("SELECTED"); ?>><?php echo($data_thn_akademik['thn']); ?></option>
<?php
		}
?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Semester</span>
					<select name="kd_semester" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
<?php
		$query_semester = "SELECT * FROM semester";
		$result_semester = mysql_query($query_semester);
		while ($data_semester =  mysql_fetch_assoc($result_semester)) {
?>
						<option value="<?php echo($data_semester['kd_semester']); ?>" <?php if($data_semester['kd_semester']==$data['kd_semester']) echo('SELECTED') ?>><?php echo($data_semester['nama_semester']); ?></option>
<?php
		}
?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama Tempat</span>
					<input type="text" class="form-control" name="nama_tempat" placeholder="Nama Tempat..." aria-describedby="sizing-addon2" value="<?php echo($data['nama_tempat']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Alamat</span>
					<textarea name="alamat" class="form-control" placeholder="Alamat..."><?php echo($data['alamat']); ?> </textarea>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kota</span>
					<input type="text" name="kota" class="form-control" placeholder="Kota..." aria-describedby="sizing-addon2" value="<?php echo($data['kota']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Provinsi</span>
					<input type="text" class="form-control" name="provinsi" placeholder="tempat lahir..." aria-describedby="sizing-addon2" value="<?php echo($data['provinsi']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Pos</span>
					<input type="text" class="form-control" name="kd_poss" placeholder="Kode Pos..." aria-describedby="sizing-addon2" value="<?php echo($data['kd_poss']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Longtitude</span>
					<input type="text" class="form-control" name="longtitude" placeholder="Longtitude..." aria-describedby="sizing-addon2" value="<?php echo($data['longtitude']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Latitude</span>
					<input type="text" class="form-control" name="latitude" placeholder="Latitude..." aria-describedby="sizing-addon2" value="<?php echo($data['latitude']); ?>">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	case 'insert':
		$id_promosi = $_POST['id_promosi'];
		$kd_thn_akademik = $_POST['kd_thn_akademik'];
		$kd_semester = $_POST['kd_semester'];
		$nama_tempat = $_POST['nama_tempat'];
		$alamat = $_POST['alamat'];
		$kota = $_POST['kota'];
		$provinsi = $_POST['provinsi'];
		$kd_poss = $_POST['kd_poss'];
		$longtitude = $_POST['longtitude'];
		$latitude = $_POST['latitude'];

		$query = "INSERT INTO promosi
						(id_promosi,kd_thn_akademik,kd_semester,nama_tempat,
						alamat,kota,provinsi,
						kd_poss,longtitude,latitude,tgl_input,titik_promosi)
					VALUES
					('$id_promosi','$kd_thn_akademik','$kd_semester','$nama_tempat',
						'$alamat','$Kota','$provinsi',
						'$kd_poss','$longtitude','$latitude',NOW(),'promosi');";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=promosi&id_promosi=$id_promosi&kode=1&do=tambah%20data");
		}
		else{
			header("Location: ?mod=promosi&id_promosi=$id_promosi&kode=2&do=tambah%20data");
		}

		break;

	case 'update':
		$id_promosi = $_GET['id_promosi'];
		$kd_thn_akademik = $_POST['kd_thn_akademik'];
		$kd_semester = $_POST['kd_semester'];
		$nama_tempat = $_POST['nama_tempat'];
		$alamat = $_POST['alamat'];
		$kota = $_POST['kota'];
		$provinsi = $_POST['provinsi'];
		$kd_poss = $_POST['kd_poss'];
		$longtitude = $_POST['longtitude'];
		$latitude = $_POST['latitude'];

		$query = "UPDATE promosi SET
						id_promosi='$id_promosi', kd_thn_akademik='$kd_thn_akademik', kd_semester='$kd_semester', nama_tempat='$nama_tempat',
						alamat='$alamat',kota='$kota',provinsi='$provinsi',
						kd_poss='$kd_poss',longtitude='$longtitude',latitude='$latitude'
					WHERE id_promosi='$id_promosi';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=promosi&id_promosi=$id_promosi&kode=1&do=edit%20data");
		}
		else{
			header("Location: ?mod=promosi&id_promosi=$id_promosi&kode=2&do=edit%20data");
		}
		break;

	case 'delete':
		$id_promosi = $_GET['id_promosi'];
		$query = "DELETE FROM promosi WHERE id_promosi='$id_promosi';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=promosi&id_promosi=$id_promosi&kode=1&do=edit%20data");
		}
		else{
			header("Location: ?mod=promosi&id_promosi=$id_promosi&kode=2&do=edit%20data");
		}
		break;

	default:
?>
<div class="panel panel-default">
	<div class="panel-heading">PROMOSI</div>
	<div class="panel-body">
		<a href="?mod=promosi&form=add">
			<button type="button" class="btn btn-default">+ Tambah Data</button>
		</a>
		<br /><br />
		<table class="table table-bordered lebihkecil" id="example" width="2500">
			<thead>
				<th class="tengah">ID Promosi</th>
				<th class="tengah">Tahun</th>
				<th class="tengah">Semster</th>
				<th class="tengah">Nama Tempat</th>
				<th class="tengah">Alamat</th>
				<th class="tengah">Kota</th>
				<th class="tengah">Provinsi</th>
				<th class="tengah">Kode Pos</th>
				<th class="tengah">Longtitude</th>
				<th class="tengah">Latitude</th>
				<th class="tengah"></th>
			</thead>
			<tbody>
<?php
		$query = "select a.*,b.thn ,c.nama_semester from promosi a INNER JOIN thn_akademik b ON a.kd_thn_akademik=b.kd_thn_akademik
					INNER JOIN semester c ON a.kd_semester=c.kd_semester";
		$result = mysql_query($query);
		while ($data = mysql_fetch_assoc($result)) {

?>
				<tr>
					<td><?php echo($data['id_promosi']); ?></td>
					<td><?php echo($data['thn']); ?></td>
					<td><?php echo($data['nama_semester']); ?></td>
					<td><?php echo($data['nama_tempat']); ?></td>
					<td><?php echo($data['alamat']); ?></td>
					<td><?php echo($data['kota']); ?></td>
					<td><?php echo($data['provinsi']); ?></td>
					<td><?php echo($data['kd_poss']); ?></td>
					<td><?php echo($data['longtitude']); ?></td>
					<td><?php echo($data['latitude']); ?></td>
					<td>
						<a title='Edit'
							href="?mod=promosi&form=edit&id_promosi=<?php echo $data['id_promosi']; ?>">
							<img src='img/edit.jpg' width='20'>
						</a>
						<a title='Hapus'
							onclick="return confirm('Anda yakin menghapus data dengan ID Promosi <?php echo $data['id_promosi'] ?> ?')"
							href="?mod=promosi&form=delete&id_promosi=<?php echo $data['id_promosi'] ?>">
							<img src='img/delete.jpg' width='20'>
						</a>
					</td>
				</tr>
<?php
		}
?>
			</tbody>
		</table>
	</div>
</div>
<?php
		break;
}

?>
