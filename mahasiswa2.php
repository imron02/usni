<?php 
$form = $_GET['form'];
echo $form;

switch ($form) {
	case 'add':
?>
<form action="?mod=mahasiswa&form=insert" method="POST" class="col-lg-8">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>MAHASISWA </strong>- Tambah Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">NIM</span>
					<input type="text" class="form-control" name="nim" placeholder="nomor induk mahasiswa..." aria-describedby="sizing-addon2" maxlength="8">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama</span>
					<input type="text" class="form-control" name="nm_mhs" placeholder="nama mahasiswa..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tahun</span>
					<select name="kd_thn_akademik" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
						<?php 
							$query_thn_akademik = "SELECT * FROM thn_akademik";
							$result_thn_akademik = mysql_query($query_thn_akademik);
							while ($data_thn_akademik =  mysql_fetch_assoc($result_thn_akademik)):
						?>
						<option value="<?php echo($data_thn_akademik['kd_thn_akademik']); ?>"><?php echo($data_thn_akademik['thn']); ?></option>
						<?php endwhile; ?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Semester</span>
					<select name="kd_semester" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
						<?php
							$query_semester = "SELECT * FROM semester";
							$result_semester = mysql_query($query_semester);
							while ($data_semester =  mysql_fetch_assoc($result_semester)):
						?>
						<option value="<?php echo($data_semester['kd_semester']); ?>"><?php echo($data_semester['nama_semester']); ?></option>
						<?php endwhile; ?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Fakultas - Jurusan</span>
					<select name="kd_jurusan" class="form-control" aria-describedby="sizing-addon2">
						<option value=""></option>
						<?php 
							$query_fakultas_jurusan = "SELECT f.*, j.kd_jurusan, j.nama_jurusan
							FROM fakultas f
							LEFT OUTER JOIN jurusan j ON f.kd_fakultas=j.kd_fakultas
							ORDER BY nama_fakultas;";
							$result_fakultas_jurusan = mysql_query($query_fakultas_jurusan);
							while ($data_fakultas_jurusan =  mysql_fetch_assoc($result_fakultas_jurusan)):
						?>
						<option value="<?php echo($data_fakultas_jurusan['kd_jurusan']); ?>"><?php echo($data_fakultas_jurusan['nama_fakultas'] . ' - ' . $data_fakultas_jurusan['nama_jurusan']); ?></option>
						<?php endwhile; ?>
					</select>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tempat Lahir</span>
					<input type="text" class="form-control" name="tempat_lahir" placeholder="tempat lahir..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Tanggal Lahir</span>
					<input type="text" class="form-control" name="tgl_lahir" placeholder="tanggal lahir format yyyy-MM-dd..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Alamat</span>
					<textarea class="form-control" name="alamat" placeholder="alamat..." ></textarea>
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kota</span>
					<input type="text" class="form-control" name="kota" placeholder="kota..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Provinsi</span>
					<input type="text" class="form-control" name="provinsi" placeholder="provinsi..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Pos</span>
					<input type="text" class="form-control" name="kd_pos" placeholder="kode pos..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">No Telepon</span>
					<input type="text" class="form-control" name="no_tlp" placeholder="no telp..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">No HP</span>
					<input type="text" class="form-control" name="no_hp" placeholder="no hp..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">email</span>
					<input type="text" class="form-control" name="email" placeholder="email..." aria-describedby="sizing-addon2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Longtitude</span>
					<input type="text" class="form-control" name="longtitude" placeholder="longtitude..." aria-describedby="sizing-addon2">
				</div>
				<br/>
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Latitude</span>
					<input type="text" class="form-control" name="latitude" placeholder="latitude..." aria-describedby="sizing-addon2">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;
	// case: 'insert':
		// print_r($_POST)
	// 	// $nim = $_POST['nim'];
	// 	// $nm_mhs = $_POST['nm_mhs'];
	// 	// $kd_thn_akademik = $_POST['kd_thn_akademik'];
	// 	// $kd_jurusan = $_POST['kd_jurusan'];
	// 	// $kd_semester = $_POST['kd_semester'];
	// 	// $tempat_lahir = $_POST['tempat_lahir'];
	// 	// $tgl_lahir = $_POST['tgl_lahir'];
	// 	// $alamat = $_POST['alamat'];
	// 	// $kota = $_POST['kota'];
	// 	// $provinsi = $_POST['provinsi'];
	// 	// $kd_pos = $_POST['kd_pos'];
	// 	// $no_tlp = $_POST['no_tlp'];
	// 	// $no_hp = $_POST['no_hp'];
	// 	// $email = $_POST['email'];
	// 	// $longtitude = $_POST['longtitude'];
	// 	// $latitude = $_POST['latitude'];
	// 	// print_r($_POST);
	// 	// $query = "INSERT INTO mahasiswa (nim,nm_mhs,kd_thn_akademik, tempat_lahir,tgl_lahir,kd_jurusan,
	// 	// 				kd_semester,alamat,kota,provinsi,kd_pos, longtitude,latitude,no_tlp,no_hp, email,
	// 	// 				password,tgl_input,titik_mhs)
	// 	// 		  VALUES ('$nim','$nm_mhs','$kd_thn_akademik', '$tempat_lahir','$tgl_lahir','$kd_jurusan',
	// 	// 				'$kd_semester','$alamat','$kota','$provinsi','$kd_pos', '$longtitude','$latitude',
	// 	// 				'$no_tlp','$no_hp', '$email','',NOW(),'mhs');";
		// break;
	default:
		# code...
		break;
}

?>

<?php  ?>