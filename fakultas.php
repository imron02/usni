<?php

switch ($_GET['form']) {
	case 'add':
?>
<form action="?mod=fakultas&form=insert" method="POST" class="col-lg-7">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>FAKULTAS </strong>- Tambah Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Fakultas</span>
					<input type="text" class="form-control" name="kd_fakultas" placeholder="kode fakultas..." aria-describedby="sizing-addon2" maxlength="2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama Fakultas</span>
					<input type="text" class="form-control" name="nama_fakultas" placeholder="nama fakultas..." aria-describedby="sizing-addon2" maxlength="50">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	case 'edit':

		$kd_fakultas = $_GET['kd_fakultas'];
		$query = "SELECT * FROM fakultas WHERE kd_fakultas='$kd_fakultas';";
		$result = mysql_query($query);
		$data = mysql_fetch_assoc($result);
?>
<form action="?mod=fakultas&form=update" method="POST" class="col-lg-7">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>FAKULTAS </strong>- Ubah Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Fakultas</span>
					<input type="text" class="form-control readonly" name="kd_fakultas" placeholder="kode fakultas..." aria-describedby="sizing-addon2" maxlength="2" readonly value="<?php echo($data['kd_fakultas']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama Fakultas</span>
					<input type="text" class="form-control" name="nama_fakultas" placeholder="nama fakultas..." aria-describedby="sizing-addon2" maxlength="50" value="<?php echo($data['nama_fakultas']); ?>">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	case 'insert':
		$kd_fakultas = $_POST['kd_fakultas'];
		$nama_fakultas = $_POST['nama_fakultas'];
		$query = "INSERT INTO fakultas (kd_fakultas,nama_fakultas) VALUES ('$kd_fakultas','$nama_fakultas');";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=fakultas&kd_fakultas=$kd_fakultas&kode=1&do=tambah%20data");
		}
		else{
			header("Location: ?mod=fakultas&kd_fakultas=$kd_fakultas&kode=2&do=tambah%20data");
		}
		break;

	case 'update':
		$kd_fakultas = $_POST['kd_fakultas'];
		$nama_fakultas = $_POST['nama_fakultas'];
		$query = "UPDATE fakultas SET nama_fakultas='$nama_fakultas' WHERE kd_fakultas='$kd_fakultas';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=fakultas&kd_fakultas=$kd_fakultas&kode=1&do=ubah%20data");
		}
		else{
			header("Location: ?mod=fakultas&kd_fakultas=$kd_fakultas&kode=2&do=ubah%20data");
		}
		break;

	case 'delete':
		$kd_fakultas = $_GET['kd_fakultas'];
		$query = "DELETE FROM fakultas WHERE kd_fakultas='$kd_fakultas';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=fakultas&kd_fakultas=$kd_fakultas&kode=1&do=hapus%20data");
		}
		else{
			header("Location: ?mod=fakultas&kd_fakultas=$kd_fakultas&kode=2&do=hapus%20data");
		}
		break;
	
	default:
?>
<div class="panel panel-default">
	<div class="panel-heading">FAKULTAS</div>
	<div class="panel-body">
		<a href="?mod=fakultas&form=add">
			<button type="button" class="btn btn-default">+ Tambah Data</button>
		</a>
		<br /><br />
		<table class="table table-bordered" id="example">
			<thead>
				<th>Kode Fakultas</th>
				<th>Nama Fakultas</th>
				<th></th>
			</thead>
			<tbody>
<?php
		$query = "SELECT * FROM fakultas";
		$result = mysql_query($query);
		while ($data = mysql_fetch_assoc($result)) {
			
?>
				<tr>
					<td><?php echo($data['kd_fakultas']); ?></td>
					<td><?php echo($data['nama_fakultas']); ?></td>
					<td>
						<a title='Edit'
							href="?mod=fakultas&form=edit&kd_fakultas=<?php echo $data['kd_fakultas']; ?>">
							<img src='img/edit.jpg' width='20'>
						</a>
						<a title='Hapus'
							onclick="return confirm('Anda yakin menghapus data dengan Kode Fakultas <?php echo $data['kd_fakultas'] ?> ?')"
							href="?mod=fakultas&form=delete&kd_fakultas=<?php echo $data['kd_fakultas'] ?>">
							<img src='img/delete.jpg' width='20'>
						</a>
					</td>
				</tr>
<?php
		}
?>
			</tbody>
		</table>
	</div>
</div>
<?php
		break;
}
?>