<?php

switch ($_GET['form']) {
	case 'add':
?>
<form action="?mod=thn_akademik&form=insert" method="POST" class="col-lg-7">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>TAHUN AKADEMIK </strong>- Tambah Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Tahun Akademik</span>
					<input type="text" class="form-control" name="kd_thn_akademik" placeholder="Kode Tahun Akademik..." aria-describedby="sizing-addon2" maxlength="2">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama Tahun Akademik</span>
					<input type="text" class="form-control" name="thn" placeholder="Format Tahun Akademik: yyyy" aria-describedby="sizing-addon2" maxlength="4">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	case 'insert':
		$kd_thn_akademik = $_POST['kd_thn_akademik'];
		$thn = $_POST['thn'];
		$query = "INSERT INTO thn_akademik (kd_thn_akademik,thn) VALUES ('$kd_thn_akademik','$thn');";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=thn_akademik&kd_thn_akademik=$kd_thn_akademik&kode=1&do=tambah%20data");
		}
		else{
			header("Location: ?mod=thn_akademik&kd_thn_akademik=$kd_thn_akademik&kode=2&do=tambah%20data");
		}
		break;

	case 'update':
		$kd_thn_akademik = $_POST['kd_thn_akademik'];
		$thn = $_POST['thn'];
		$query = "UPDATE thn_akademik SET thn='$thn' WHERE kd_thn_akademik='$kd_thn_akademik';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=thn_akademik&kd_thn_akademik=$kd_thn_akademik&kode=1&do=ubah%20data");
		}
		else{
			header("Location: ?mod=thn_akademik&kd_thn_akademik=$kd_thn_akademik&kode=2&do=ubah%20data");
		}
		break;

	case 'delete':
		$kd_thn_akademik = $_GET['kd_thn_akademik'];
		$query = "DELETE FROM thn_akademik WHERE kd_thn_akademik='$kd_thn_akademik';";
		$result = mysql_query($query);
		if ($result) {
			header("Location: ?mod=thn_akademik&kd_thn_akademik=$kd_thn_akademik&kode=1&do=hapus%20data");
		}
		else{
			header("Location: ?mod=thn_akademik&kd_thn_akademik=$kd_thn_akademik&kode=2&do=hapus%20data");
		}
		break;

	case 'edit':

		$kd_thn_akademik = $_GET['kd_thn_akademik'];
		$query = "SELECT * FROM thn_akademik WHERE kd_thn_akademik='$kd_thn_akademik';";
		$result = mysql_query($query);
		$data = mysql_fetch_assoc($result);
?>
<form action="?mod=thn_akademik&form=update" method="POST" class="col-lg-7">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>TAHUN AKADEMIK </strong>- Ubah Data</div>
		<div class="panel-body">
			<table class="form">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Kode Tahun Akademik</span>
					<input type="text" class="form-control readonly" name="kd_thn_akademik" placeholder="Kode Tahun Akademik..." aria-describedby="sizing-addon2" maxlength="2" readonly value="<?php echo($data['kd_thn_akademik']); ?>">
				</div>
				<br />
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon2">Nama Tahun Akademik</span>
					<input type="text" class="form-control" name="thn" placeholder="Nama Tahun Akademik..." aria-describedby="sizing-addon2" maxlength="10" value="<?php echo($data['thn']); ?>">
				</div>
			</table>
		</div>
		<div class="panel-footer">
			<button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</div>
</form>
<?php
		break;

	default:
?>
<div class="panel panel-default">
	<div class="panel-heading">TAHUN AKADEMIK</div>
	<div class="panel-body">
		<a href="?mod=thn_akademik&form=add">
			<button type="button" class="btn btn-default">+ Tambah Data</button>
		</a>
		<br /><br />
		<table class="table table-bordered" id="example">
			<thead>
				<th>Kode Tahun Akademik</th>
				<th>Nama Tahun Akademik</th>
				<th></th>
			</thead>
			<tbody>
<?php
		$query = "SELECT * FROM thn_akademik";
		$result = mysql_query($query);
		while ($data = mysql_fetch_assoc($result)) {
			
?>
				<tr>
					<td><?php echo($data['kd_thn_akademik']); ?></td>
					<td><?php echo($data['thn']); ?></td>
					<td>
						<a title='Edit'
							href="?mod=thn_akademik&form=edit&kd_thn_akademik=<?php echo $data['kd_thn_akademik']; ?>">
							<img src='img/edit.jpg' width='20'>
						</a>
						<a title='Hapus'
							onclick="return confirm('Anda yakin menghapus data dengan Kode Tahun Akademik <?php echo $data['kd_thn_akademik'] ?> ?')"
							href="?mod=thn_akademik&form=delete&kd_thn_akademik=<?php echo $data['kd_thn_akademik'] ?>">
							<img src='img/delete.jpg' width='20'>
						</a>
					</td>
				</tr>
<?php
		}
?>
			</tbody>
		</table>
	</div>
</div>
<?php
		break;
}

?>